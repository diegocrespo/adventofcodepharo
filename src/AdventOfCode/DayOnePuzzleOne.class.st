"
I am a class that holds all the code to complete the puzzles for the day one of the advent of code\
To complete the first problem run this code

""DayOnePuzzleOne new buildTeam mostCalories.""
"
Class {
	#name : #DayOnePuzzleOne,
	#superclass : #Object,
	#instVars : [
		'elfTeam'
	],
	#category : #AdventOfCode
}

{ #category : #accessing }
DayOnePuzzleOne >> buildTeam [
	" Create a team of elves using the total calories each elf has in their pack"
	elfTeam := self collectCalories collect: [ :cal| Elf packFood: cal]
]

{ #category : #accessing }
DayOnePuzzleOne >> collectCalories [
	" Reads in a file of calories per elf. an empty line symbolizes that the subsequent set of calories
		belongs to a new elf. Returns an OrderedCollection of elves initialized with total number of
		calories"
	| elfFile fileName string stringArray tempCollect finalCollect|
	fileName := 'C:\Users\decre\Downloads\elfCalories'.
	elfFile := fileName asFileReference.
	string := elfFile readStream upToEnd.
	stringArray := string lines.

	tempCollect := OrderedCollection new.
	finalCollect := OrderedCollection new.
	stringArray do: [ :str |
    	str = '' ifFalse: [ tempCollect add: str asInteger ]
    	ifTrue: [ finalCollect add: tempCollect.
        	tempCollect := OrderedCollection new] ].
	^ finalCollect
]

{ #category : #accessing }
DayOnePuzzleOne >> initialize [
	self collectCalories
]

{ #category : #accessing }
DayOnePuzzleOne >> mostCalories [
	^ (elfTeam collect: [ :elves | elves maxCalories ]) max
	
]

{ #category : #accessing }
DayOnePuzzleOne >> topThreeCalories [
	"Get the total number of calories from the top3 elves"
	|calories|
	calories := OrderedCollection new.
	elfTeam do: [ :elf | calories add: elf maxCalories ].
	calories := calories sort: [ :cal1 :cal2 | cal1 >= cal2 ].
	^ (calories first: 3) inject: 0 into: [ :num1 :num2 | num1 + num2 ].
]
