"
I am a class that holds the solution to the DayTwoPuzzles
"
Class {
	#name : #DayTwoPuzzle,
	#superclass : #Object,
	#instVars : [
		'strategy',
		'pointsList'
	],
	#category : #AdventOfCode
}

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> calculateHands [
	"Read in the file calculate which hand I need to lose win or draw"
	|newStrategy|
	newStrategy := OrderedCollection new.
	strategy do: [ :str | | hand1 hand2 |
				hand1 := str allButLast.
				hand2 := str allButFirst.
				"Elf plays Rock"
				(hand1 = 'A' and: hand2 = 'X') ifTrue: [ newStrategy add: 'AZ' ].
				(hand1 = 'A' and: hand2 = 'Y') ifTrue: [ newStrategy add: 'AX'  ].
				(hand1 = 'A' and: hand2 = 'Z') ifTrue: [ newStrategy add: 'AY' ].
					"Elf plays Paper"
				(hand1 = 'B' and: hand2 = 'X') ifTrue: [ newStrategy add: 'BX' ].
				(hand1 = 'B' and: hand2 = 'Y') ifTrue: [ newStrategy add: 'BY' ].
				(hand1 = 'B' and: hand2 = 'Z') ifTrue: [ newStrategy add: 'BZ' ].
					"Elf plays Scissors"
				(hand1 = 'C' and: hand2 = 'X') ifTrue: [ newStrategy add: 'CY' ].
				(hand1 = 'C' and: hand2 = 'Y') ifTrue: [ newStrategy add: 'CZ' ].
				(hand1 = 'C' and: hand2 = 'Z') ifTrue: [ newStrategy add: 'CX' ].
				 ].
			strategy := newStrategy
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> calculatePoints [
	"Read in the file and calculate total number of winning poitns in rock paper scissors"
	pointsList := OrderedCollection new.
	strategy do: [ :str | |hand1 hand2 choice win draw|
				hand1 := str allButLast.
				hand2 := str allButFirst.
				win := 6.
				draw := 3.
				hand2 = 'X' ifTrue: [ choice := 1 ]."Rock"
				hand2 = 'Y' ifTrue: [ choice := 2 ]. "Paper"
				hand2 = 'Z' ifTrue: [ choice := 3 ]. "Scissors"
				"Elf plays Rock"
				(hand1 = 'A' and: hand2 = 'X') ifTrue: [ pointsList add: choice + draw ].
				(hand1 = 'A' and: hand2 = 'Y') ifTrue: [ pointsList add: choice + win  ].
				(hand1 = 'A' and: hand2 = 'Z') ifTrue: [ pointsList add: choice ].
					"Elf plays Paper"
				(hand1 = 'B' and: hand2 = 'X') ifTrue: [ pointsList add: choice ].
				(hand1 = 'B' and: hand2 = 'Y') ifTrue: [ pointsList add: choice + draw  ].
				(hand1 = 'B' and: hand2 = 'Z') ifTrue: [ pointsList add: choice + win ].
					"Elf plays Scissors"
				(hand1 = 'C' and: hand2 = 'X') ifTrue: [ pointsList add: choice + win  ].
				(hand1 = 'C' and: hand2 = 'Y') ifTrue: [ pointsList add: choice ].
				(hand1 = 'C' and: hand2 = 'Z') ifTrue: [ pointsList add: choice + draw].
				 ].
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> collectPoints [
	"Read in the files and calculate the total wins"
	| elfFile fileName string stringArray tempCollect finalCollect|
	fileName := 'C:\Users\decre\Downloads\rps_strategy.txt'.
	elfFile := fileName asFileReference.
	string := elfFile readStream upToEnd.
	stringArray := string lines.
	strategy := stringArray collect: [ :str | str onlyLetters ]
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> initialize [
	super initialize.
	pointsList := OrderedCollection new.
	self collectPoints.
	self calculatePoints.
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> pointsList [
	^ pointsList
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> strategy [
	^ strategy
]

{ #category : #'as yet unclassified' }
DayTwoPuzzle >> sumPoints [
	^ pointsList inject: 0 into: [ :num1 :num2 | num1 + num2 ]
]
