"
I am an elf. I have an inventory which contains the number of calories for each snack I pack
"
Class {
	#name : #Elf,
	#superclass : #Object,
	#instVars : [
		'inventory'
	],
	#category : #AdventOfCode
}

{ #category : #'as yet unclassified' }
Elf class >> packFood: anArray [
^ self new inventory: anArray
]

{ #category : #accessing }
Elf >> inventory [
	^ inventory
]

{ #category : #accessing }
Elf >> inventory: anArray [
	inventory := anArray
]

{ #category : #accessing }
Elf >> maxCalories [
	^ inventory sum
]
