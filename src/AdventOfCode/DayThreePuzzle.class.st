"
I am the holder of the methods to solve day three of the advent of code
"
Class {
	#name : #DayThreePuzzle,
	#superclass : #Object,
	#instVars : [
		'packList',
		'compartmentCount'
	],
	#category : #AdventOfCode
}

{ #category : #'as yet unclassified' }
DayThreePuzzle >> compartmentCount [
	^ compartmentCount 
]

{ #category : #'as yet unclassified' }
DayThreePuzzle >> initialize [
	super initialize.
	self readPacks.
	self searchPacks. 
]

{ #category : #'as yet unclassified' }
DayThreePuzzle >> packList [
	^ packList
]

{ #category : #'as yet unclassified' }
DayThreePuzzle >> readPacks [
	| fileName |
	fileName := 'C:\Users\decre\packs.txt'.
	packList := fileName asFileReference readStream upToEnd lines.
	
	
]

{ #category : #'as yet unclassified' }
DayThreePuzzle >> searchPacks [
	|splitPack letterIndex|
	compartmentCount := OrderedCollection new.
	splitPack := OrderedCollection new.
	letterIndex := 'abcdefghijklmnopqrstuvwxyz' , 'abcdefghijklmnopqrstuvwxyz' asUppercase.
	packList do: [ :inv | |comp1 comp2 numberOfItems overlap|
			numberOfItems := inv size.
			comp1 := inv first: numberOfItems / 2.
			comp2 := inv last: numberOfItems / 2.
			overlap := (Set withAll: comp1) intersection: (Set withAll: comp2).
			overlap do: [ :char | compartmentCount add: (letterIndex indexOf: char) ].
	].
	^ compartmentCount
]

{ #category : #'as yet unclassified' }
DayThreePuzzle >> sumContents [
	^ compartmentCount inject: 0 into: [ :num1 :num2 | num1 + num2 ]
]
