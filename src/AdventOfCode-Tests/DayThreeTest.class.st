Class {
	#name : #DayThreeTest,
	#superclass : #TestCase,
	#category : #'AdventOfCode-Tests'
}

{ #category : #tests }
DayThreeTest >> testSumContents [
	self assert: DayThreePuzzle new sumContents equals: 8072.
]
