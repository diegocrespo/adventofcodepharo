Class {
	#name : #DayTwoTest,
	#superclass : #TestCase,
	#category : #'AdventOfCode-Tests'
}

{ #category : #tests }
DayTwoTest >> testTotalPoints [
	self assert: DayTwoPuzzle new sumPoints equals: 8890.
]

{ #category : #tests }
DayTwoTest >> testTotalPointsDeterminedHands [
self assert: DayTwoPuzzle new calculateHands calculatePoints sumPoints equals: 10238
]
