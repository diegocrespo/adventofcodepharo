"
I am a class to test the DayOne advent of code problems
"
Class {
	#name : #DayOneTest,
	#superclass : #TestCase,
	#category : #'AdventOfCode-Tests'
}

{ #category : #tests }
DayOneTest >> testMaxCalories [
	|e|
	e := Elf packFood: #(1000 2000 3000).
	self assert: e maxCalories equals: 6000.
]

{ #category : #tests }
DayOneTest >> testMostCalories [
	 self assert: DayOnePuzzleOne new buildTeam mostCalories equals: 67027.
]

{ #category : #tests }
DayOneTest >> testSetInventory [
	|elf|
	elf := Elf packFood: #(1000 2000 3000). 

	self assert: elf inventory size equals: 3. 
]

{ #category : #tests }
DayOneTest >> testTopThree [
	| puzz |
	puzz := DayOnePuzzleOne new buildTeam.
	self assert: puzz topThreeCalories equals: 197291.
]
